import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Parent1Module } from './parent1/parent1.module'; 
import { Parent2Module } from './parent2/parent2.module';
import { ParentModule } from './lifecycle-hooks/parent/parent.module';
import { DataService } from './data.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    Parent1Module,
    Parent2Module,
    ParentModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
