import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  name: string = 'Angular Components';
  componentCommunication: string =
    'Data Communication Between Components Example';
  lifeCycleHooks: string = 'Life Cycle Hooks Example';
}
