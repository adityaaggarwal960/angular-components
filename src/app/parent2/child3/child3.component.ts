import { Component } from '@angular/core';
import { DataService } from '../../data.service';

@Component({
  selector: 'app-child3',
  templateUrl: './child3.component.html',
  styleUrls: ['./child3.component.scss'],
})
export class Child3Component {
  //Example of data sharing between Parent and child component using @ViewChild() decorator

  //child3Method() is used as a reference function which is used in the parent2 component
  child3Method() {
    console.log('Method in child3 component');
  }

  //Example of data sharing between siblings component(components who does not have direct relation between them).

  //constructor is used to use the services in angular. services provides us a communication between the components
  constructor(private dataService: DataService) {}

  //get data() is used to get the data in the service which is shared by child1 component
  get data(): string {
    return this.dataService.data.age + ' ' + this.dataService.data.name;
  }

  //set data(newName) is used to set the name in the service which is shared by child1 component
  set data(newName: string) {
    this.dataService.data.name = newName;
  }
}
