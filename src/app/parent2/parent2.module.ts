import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { Parent2Component } from './parent2.component';
import { Child3Component } from './child3/child3.component';

@NgModule({
  declarations: [Parent2Component, Child3Component],
  imports: [BrowserModule],
  providers: [],
  exports: [Parent2Component],
})
export class Parent2Module {}
