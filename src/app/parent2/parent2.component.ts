import { Component, ViewChild } from '@angular/core';
import { Child3Component } from './child3/child3.component';

@Component({
  selector: 'app-parent2',
  templateUrl: './parent2.component.html',
  styleUrls: ['./parent2.component.scss'],
})
export class Parent2Component {
  //Example of data sharing between Parent and child component using @ViewChild() decorator

  //@ViewChild() decorator is used to call the whole child components including all it methods, functions, variables,etc
  @ViewChild(Child3Component) child3Component!: Child3Component;

  public message: any;

  ngAfterViewInit() {
    if (this.child3Component) {
      // child3Method is called as an object of this.child3Component here
      this.child3Component.child3Method();
    }
  }
}
