import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-child2',
  templateUrl: './child2.component.html',
  styleUrls: ['./child2.component.scss'],
})
export class Child2Component {
  //Example of data sharing between Parent and child component using @Output() decorator
  
  //@Output() decorator is used to give output to parent1 as messageFromChild2
  @Output() messageFromChild2 = new EventEmitter();

  //sendMessageToParent1() function is used to emit the data which the user wants to emit from child2 component to parent1 component
  sendMessageToParent1() {
    this.messageFromChild2.emit('Message sent from child2 to parent1');
  }
}
