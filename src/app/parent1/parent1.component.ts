import { Component } from '@angular/core';

@Component({
  selector: 'app-parent1',
  templateUrl: './parent1.component.html',
  styleUrls: ['./parent1.component.scss'],
})
export class Parent1Component {
  //Example of data sharing between Parent and child component using @Input() and @Ouput() decorator

  parent1MessageToChild1: string = 'Message sent from parent1 to child1';
  parent1MessageFromChild2: string = '';

  // onMessageRecievedFromChild2() function is used to set value of event(recieved from child2) in parent1MessageFromChild2
  onMessageRecievedFromChild2(event: any) {
    console.log(event);
    this.parent1MessageFromChild2 = event;
  }
}
