import { Component, Input } from '@angular/core';
import { DataService } from '../../data.service';
@Component({
  selector: 'app-child1',
  templateUrl: './child1.component.html',
  styleUrls: ['./child1.component.scss'],
})
export class Child1Component {
  //Example of data sharing between Parent and child component using @Input() decorator

  //@Input() decorator is used to get input from parent1 as child1MessageFromParent1
  @Input() child1MessageFromParent1: any;

  //Example of data sharing between siblings component(components who does not have direct relation between them).

  //constructor is used to use the services in angular. services provides us a communication between the components
  constructor(public dataService: DataService) {}

  //get data() is used to get the data in the service which is shared by child3 component
  get data(): string {
    return this.dataService.data.age + ' ' + this.dataService.data.name;
  }
  //set data(newName) is used to set the name in the service which is shared by child1 component
  set data(newName: string) {
    this.dataService.data.name = newName;
  }
}
