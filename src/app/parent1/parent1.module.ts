import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { Parent1Component } from './parent1.component';
import { Child1Component } from './child1/child1.component';
import { Child2Component } from './child2/child2.component';

@NgModule({
  declarations: [Parent1Component, Child1Component, Child2Component],
  imports: [BrowserModule],
  providers: [],
  exports: [Parent1Component],
})
export class Parent1Module {}
