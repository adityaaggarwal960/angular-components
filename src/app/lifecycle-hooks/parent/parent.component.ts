import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.scss'],
})
export class ParentComponent implements OnInit {
  data = 0;
  user = {
    name: 'Parent User 1',
  };
  showChild = true;

  constructor() {}

  //changeFromParent() function will increment value of data by 1 in UI
  changeFromParent() {
    this.data += 1;
  }

  //updateUser() is used to update the username
  updateUser() {
    this.user.name = 'Parent name is changed to Parent User 2';
  }

  //clickMe() function is called when the user clicks on Click me
  clickMe() {
    console.log('clickMe function is called');
  }

  //update() function will destroys" the child component via the conditional ngIf='showChild'.
  update() {
    this.showChild = !this.showChild;
  }

  //ngOnInit() is called once whenever the component is initialized.
  //ngOnInit() executes once after the first ngOnChanges.
  ngOnInit() {
    console.log('ngOnInit called in ParentComponent');
  }

  //ngAfterContentInit() runs once after the first ngDoCheck().
  //ngAfterContentInit() runs after ngOnInit and ngDoCheck().
  ngAfterContentInit() {
    console.log('ngAfterContentInit called');
  }

  //ngAfterContentChecked() is called directly after ngAfterContentInit().
  //ngAfterContentChecked() is called after every subsequent ngDoCheck().
  ngAfterContentChecked() {
    console.log('ngAfterContentChecked called');
  }

  //ngAfterViewInit() is called once after ngAfterContentChecked().
  //ngAfterViewInit() gets called one time after ngDoCheck().
  ngAfterViewInit() {
    console.log('ngAfterViewInit called');
  }

  //ngAfterViewChecked() is called after ngAfterContentInit().
  //ngAfterViewChecked() is called after every subsequent ngAfterContentChecked().
  ngAfterViewChecked() {
    console.log('ngAfterViewChecked called');
  }
}
