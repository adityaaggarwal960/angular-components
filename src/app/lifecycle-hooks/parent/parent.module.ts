import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ParentComponent } from './parent.component';
import { ChildComponent } from './child/child.component';

@NgModule({
  declarations: [ParentComponent, ChildComponent],
  imports: [BrowserModule],
  providers: [],
  exports: [ParentComponent],
})
export class ParentModule {}
