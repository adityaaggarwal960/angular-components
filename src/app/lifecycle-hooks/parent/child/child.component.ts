import { Component, OnInit, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss'],
})
export class ChildComponent implements OnInit {
  @Input() parentData: any;
  @Input() username!: any;

  //changeFromChild decrement the value of data by 1 in UI which is an input here from the parent component
  changeFromChild() {
    this.parentData -= 1;
  }

  //ngOnInit is called whenever the component is loaded
  ngOnInit() {
    console.log('ngOnInit called in ChildComponent');
  }

  //ngOnChanges is called whenever the changeFromChild function is called
  ngOnChanges(changes: SimpleChanges) {
    console.log(changes, 'ngOnChanges called');
  }

  //ngDoCheck() is called whenever change detection is run.
  //ngDoCheck() is called immediately after ngOnChanges() and ngOnInit()
  ngDoCheck() {
    console.log('ngDoCheck called');
  }

  //ngOnDestroy() gets called when a component is about to be destroyed.
  //ngOnDestroy() will destroy this child component via the conditional ngIf='showChild' in the parent component.
  ngOnDestroy() {
    console.log('ngOnDestroy called');
  }
}
